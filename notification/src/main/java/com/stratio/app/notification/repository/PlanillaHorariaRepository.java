package com.stratio.app.notification.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.stratio.app.notification.entity.PlanillaHoraria;
import com.stratio.app.notification.model.PlanillaInfoModel;


@Repository("planillaHorariaRepository")
public interface PlanillaHorariaRepository extends JpaRepository<PlanillaHoraria, Serializable> {
	
	@Query("select new com.stratio.app.notification.model.PlanillaInfoModel"
			+ "( "
			+ "cast(ph.idPlanillaHoraria as text), "
			+ "cast(ph.fechaActual as text),		  "
			+ "cast(ph.horaEntrada as text),       "
			+ "cast(ph.horaSalida as text),        "
			+ "cast(ph.horaAusencia as text),      "
			+ "cast(ph.horasTrabajadas as text),   "
			+ "cast(e.idEmpleado as text),        "
			+ "cast(p.nombre as text),            "
			+ "cast(p.apellidoPaterno as text),   "
			+ "cast(p.apellidoMaterno as text),   "
			+ "cast(d.nombreDepartamento as text) "
			+ ") "
			+ "from PlanillaHoraria ph, Empleado e, Persona p, Departamento d "
			+ "where ph.idEmpleado = e.idEmpleado "
			+ "and e.persona.idPersona = p.idPersona "
			+ "and e.departamento.idDepartamento = d.idDepartamento")
	List<PlanillaInfoModel> consultarPlanillas();
	
	List<PlanillaHoraria> findByIdEmpleado(@Param("idEmpleado") Integer idEmpleado);
	
	PlanillaHoraria findTopByIdEmpleadoOrderByFechaActual(@Param("idEmpleado") Integer idEmpleado);
	

}

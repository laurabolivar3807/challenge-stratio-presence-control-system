package com.stratio.app.notification.controller;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.stratio.app.notification.model.EmpleadoModel;
import com.stratio.app.notification.service.EmpleadoService;

import reactor.core.publisher.Flux;

/**
 * 
 * @author Luis Alberto López Lombana
 *
 */
@RestController
@RequestMapping("/empleados")
public class EmpleadoRestController implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7056933607089578946L;
	@Autowired
	@Qualifier("empleadoServiceImpl")
	protected EmpleadoService empleadoService;

	@GetMapping(path = "/findAllEmpleados", produces = "application/stream+json")
	public Flux<EmpleadoModel> uploadFileMulti() {
		return empleadoService.obtenerListadoEmpleados();
	}
	
	@GetMapping(path = "/accesoEmpleado/{idEmpleado}")
	@ResponseBody
	public String accesoEmpleado(@PathVariable Integer idEmpleado) {
		return empleadoService.accesoEmpleado(idEmpleado);
	}
	
	@GetMapping(path = "/salidaEmpleado/{idEmpleado}")
	@ResponseBody
	public String salidaEmpleado(@PathVariable Integer idEmpleado) {
		return empleadoService.salidaEmpleado(idEmpleado);
	}

}

package com.stratio.app.notification.enumerador;

public enum EstadoOperaciones {

	INICIAR(1), EXITO(2), ERROR(3);

	private final Integer operationId;

	EstadoOperaciones(Integer operationId) {

		this.operationId = operationId;
	}

	public Integer getOperationId() {
		return operationId;

	}

}

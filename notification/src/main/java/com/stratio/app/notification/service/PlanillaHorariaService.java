package com.stratio.app.notification.service;

import com.stratio.app.notification.entity.PlanillaHoraria;
import com.stratio.app.notification.enumerador.EstadoOperaciones;
import com.stratio.app.notification.model.PlanillaInfoModel;

import reactor.core.publisher.Flux;

/**
 * 
 * @author Luis Alberto López Lombana
 *
 */
public interface PlanillaHorariaService extends java.io.Serializable {
	PlanillaHoraria guardarPlanillaHoraria(PlanillaHoraria planillaHoraria);

	Flux<PlanillaInfoModel> consultarPlanillas();

	Flux<EstadoOperaciones> recepcionNotificacion(PlanillaInfoModel planillaInfoModel); 
}

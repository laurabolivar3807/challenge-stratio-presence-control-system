package com.stratio.app.notification.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the empleado database table.
 * 
 */
@Entity
@NamedQuery(name = "Empleado.findAll", query = "SELECT e FROM Empleado e")
public class Empleado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue()
	@Column(name = "id_empleado")
	private Integer idEmpleado;

	private String cargo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_creacion")
	private Date fechaCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_ingreso")
	private Date fechaIngreso;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_modificacion")
	private Date fechaModificacion;

	private BigDecimal sueldo;

	
	@Column(name = "usuario_creacion")
	private Integer usuarioCreacion;

	
	@Column(name = "usuario_modificacion")
	private Integer usuarioModificacion;

	// bi-directional many-to-one association to Departamento
	@ManyToOne
	@JoinColumn(name = "id_departamento")
	private Departamento departamento;

	// bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name = "id_persona")
	private Persona persona;

	// bi-directional many-to-one association to PlanillaHoraria
	@ManyToOne
	@JoinColumn(name = "id_planilla_horaria")
	private PlanillaHoraria planillaHoraria;

	public Empleado() {
	}

	public Integer getIdEmpleado() {
		return this.idEmpleado;
	}

	public void setIdEmpleado(Integer idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public String getCargo() {
		return this.cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaIngreso() {
		return this.fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public BigDecimal getSueldo() {
		return this.sueldo;
	}

	public void setSueldo(BigDecimal sueldo) {
		this.sueldo = sueldo;
	}

	public Integer getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(Integer usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Integer getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(Integer usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public Departamento getDepartamento() {
		return this.departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public PlanillaHoraria getPlanillaHoraria() {
		return this.planillaHoraria;
	}

	public void setPlanillaHoraria(PlanillaHoraria planillaHoraria) {
		this.planillaHoraria = planillaHoraria;
	}

	@Override
	public String toString() {
		return "Empleado [idEmpleado=" + idEmpleado + ", cargo=" + cargo + ", fechaCreacion=" + fechaCreacion
				+ ", fechaIngreso=" + fechaIngreso + ", fechaModificacion=" + fechaModificacion + ", sueldo=" + sueldo
				+ ", usuarioCreacion=" + usuarioCreacion + ", usuarioModificacion=" + usuarioModificacion
				+ ", departamento=" + departamento + ", persona=" + persona + ", planillaHoraria=" + planillaHoraria
				+ "]";
	}

	
}
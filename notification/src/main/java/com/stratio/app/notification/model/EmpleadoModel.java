package com.stratio.app.notification.model;

import java.time.LocalDateTime;

/**
 * 
 * @author Luis Alberto López Lombana
 *
 */
public class EmpleadoModel extends PersonaModel {

	private Integer idEmpleado;
	private LocalDateTime fechaIngreso;
	private String cargo;
	private Double sueldo;
	private DepartamentoModel departamento;
	private PlanillaHorariaModel planillaHoraria;

	public EmpleadoModel() {
	}

	public EmpleadoModel(Integer idEmpleado, LocalDateTime fechaIngreso, String cargo, Double sueldo,
			DepartamentoModel departamento, PlanillaHorariaModel planillaHoraria) {
		super();
		this.idEmpleado = idEmpleado;
		this.fechaIngreso = fechaIngreso;
		this.cargo = cargo;
		this.sueldo = sueldo;
		this.departamento = departamento;
		this.planillaHoraria = planillaHoraria;
	}

	public Integer getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(Integer idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public LocalDateTime getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(LocalDateTime fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public Double getSueldo() {
		return sueldo;
	}

	public void setSueldo(Double sueldo) {
		this.sueldo = sueldo;
	}

	public DepartamentoModel getDepartamento() {
		return departamento;
	}

	public void setDepartamento(DepartamentoModel departamento) {
		this.departamento = departamento;
	}

	public PlanillaHorariaModel getPlanillaHoraria() {
		return planillaHoraria;
	}

	public void setPlanillaHoraria(PlanillaHorariaModel planillaHoraria) {
		this.planillaHoraria = planillaHoraria;
	}

}

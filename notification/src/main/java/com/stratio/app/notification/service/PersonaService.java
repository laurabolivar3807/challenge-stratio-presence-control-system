package com.stratio.app.notification.service;

import com.stratio.app.notification.entity.Persona;

/**
 * 
 * @author Luis Alberto López Lombana
 *
 */
public interface PersonaService extends java.io.Serializable {
	Persona guardarPersona(Persona persona);
}

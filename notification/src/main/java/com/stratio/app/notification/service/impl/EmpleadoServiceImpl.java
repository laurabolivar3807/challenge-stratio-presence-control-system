package com.stratio.app.notification.service.impl;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.stratio.app.notification.converter.DefaultConverter;
import com.stratio.app.notification.entity.Empleado;
import com.stratio.app.notification.entity.PlanillaHoraria;
import com.stratio.app.notification.model.EmpleadoModel;
import com.stratio.app.notification.service.EmpleadoService;

import static java.time.temporal.ChronoUnit.MINUTES;
import reactor.core.publisher.Flux;

@Service("empleadoServiceImpl")
public class EmpleadoServiceImpl extends InjectionRepository implements EmpleadoService {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8977397560896031295L;
	private static final Logger LOGGER = LoggerFactory.getLogger(EmpleadoServiceImpl.class);

	@Override
	public Flux<EmpleadoModel> obtenerListadoEmpleados() {
		List<EmpleadoModel> lstEmpleadoModel = new ArrayList<>();
		empleadoRepository.findAll()
				.forEach(e -> lstEmpleadoModel.add(DefaultConverter.convertEmpleadoToEmpleadoModel(e)));
		Flux<EmpleadoModel> flux = Flux.fromIterable(lstEmpleadoModel).delayElements(Duration.ofSeconds(1));
		flux.subscribe(x -> LOGGER.info(x.toString()));
		return flux;
	}

	@Override
	public Empleado guardarEmpleado(Empleado empleado) {
		return empleadoRepository.save(empleado);
	}

	@Override
	public String accesoEmpleado(Integer idEmpleado) {
		Optional<Empleado> empleado = empleadoRepository.findById(idEmpleado);
		if(empleado.isPresent()) {
			PlanillaHoraria planillaHoraria = empleado.get().getPlanillaHoraria();
			
			LocalTime horaSalida = planillaHoraria.getHoraSalida();
			if(horaSalida!=null) {
				planillaHoraria.setHoraAusencia(MINUTES.between(LocalTime.now(), horaSalida));
				planillaHoraria = new PlanillaHoraria();
			}
			
			planillaHoraria.setFechaActual(new Date());
			planillaHoraria.setHoraEntrada(LocalTime.now());
			planillaHoraria.setIdEmpleado(empleado.get().getIdEmpleado());
			registroAuditoria(planillaHoraria);
			planillaHoraria = planillaHorariaRepository.save(planillaHoraria);
			empleado.get().setPlanillaHoraria(planillaHoraria);
			empleadoRepository.save(empleado.get());
			return "Ingreso concedido";
		}else {
			return "Ingreso rechazado";
		}
		
	}

	@Override
	public String salidaEmpleado(Integer idEmpleado) {
		Optional<Empleado> empleado = empleadoRepository.findById(idEmpleado);
		if(empleado.isPresent()) {
			PlanillaHoraria planillaHoraria = empleado.get().getPlanillaHoraria();
			planillaHoraria.setFechaActual(new Date());
			planillaHoraria.setHoraSalida(LocalTime.now());
			planillaHoraria.setIdEmpleado(empleado.get().getIdEmpleado());
			registroAuditoria(planillaHoraria);
			planillaHoraria.setNovedades(new ArrayList<>());
			planillaHorariaRepository.save(planillaHoraria);
			return "Salida concedida";
		}else {
			return "Salida rechazada";
		}
		
	}
	
	private void registroAuditoria(PlanillaHoraria planillaHoraria) {
		planillaHoraria.setUsuarioCreacion(9999999);
		planillaHoraria.setFechaCreacion(new Date());
		planillaHoraria.setUsuarioModificacion(9999999);
		planillaHoraria.setFechaModificacion(new Date());
	}

}

package com.stratio.app.notification.model;

import java.time.LocalDateTime;

/**
 * 
 * @author Luis Alberto López Lombana
 *
 */
public class NovedadModel {

	private String nombreNovedad;
	private String descripcionNovedad;
	private LocalDateTime fechaNovedad;
	private Integer idUsuarioReporta;

	public NovedadModel() {
	}

	public NovedadModel(String nombreNovedad, String descripcionNovedad, LocalDateTime fechaNovedad,
			Integer idUsuarioReporta) {
		this.nombreNovedad = nombreNovedad;
		this.descripcionNovedad = descripcionNovedad;
		this.fechaNovedad = fechaNovedad;
		this.idUsuarioReporta = idUsuarioReporta;
	}

	public String getNombreNovedad() {
		return nombreNovedad;
	}

	public void setNombreNovedad(String nombreNovedad) {
		this.nombreNovedad = nombreNovedad;
	}

	public String getDescripcionNovedad() {
		return descripcionNovedad;
	}

	public void setDescripcionNovedad(String descripcionNovedad) {
		this.descripcionNovedad = descripcionNovedad;
	}

	public LocalDateTime getFechaNovedad() {
		return fechaNovedad;
	}

	public void setFechaNovedad(LocalDateTime fechaNovedad) {
		this.fechaNovedad = fechaNovedad;
	}

	public Integer getIdUsuarioReporta() {
		return idUsuarioReporta;
	}

	public void setIdUsuarioReporta(Integer idUsuarioReporta) {
		this.idUsuarioReporta = idUsuarioReporta;
	}

}

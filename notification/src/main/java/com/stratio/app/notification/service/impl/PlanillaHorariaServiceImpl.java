package com.stratio.app.notification.service.impl;

import java.time.Duration;
import java.util.Date;
import java.util.function.Consumer;

import org.springframework.stereotype.Service;

import com.stratio.app.notification.entity.Novedad;
import com.stratio.app.notification.entity.PlanillaHoraria;
import com.stratio.app.notification.enumerador.EstadoOperaciones;
import com.stratio.app.notification.model.PlanillaInfoModel;
import com.stratio.app.notification.service.PlanillaHorariaService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
/**
 * 
 * @author Luis Alberto López Lombana
 *
 */
@Service("planillaHorariaServiceImpl")
public class PlanillaHorariaServiceImpl extends InjectionRepository implements PlanillaHorariaService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1479523695180782368L;

	@Override
	public PlanillaHoraria guardarPlanillaHoraria(PlanillaHoraria planillaHoraria) {
		return planillaHorariaRepository.save(planillaHoraria);
	}

	@Override
	public Flux<PlanillaInfoModel> consultarPlanillas() {
		Flux<PlanillaInfoModel> flux = Flux.fromIterable(planillaHorariaRepository.consultarPlanillas())
				.delayElements(Duration.ofSeconds(1));
		flux.subscribe(e->{
			
		});
		return flux;
	}
	
	@Override
	public Flux<EstadoOperaciones> recepcionNotificacion(PlanillaInfoModel planillaInfoModel) {
		PlanillaHoraria planillaHoraria =planillaHorariaRepository.findTopByIdEmpleadoOrderByFechaActual(Integer.parseInt(planillaInfoModel.getIdEmpleado()));
		Flux<PlanillaHoraria> flux = Flux.just(planillaHoraria);
		// async block
	    Consumer<? super FluxSink<EstadoOperaciones>> estadoEmision = stream -> {
	        stream.next(EstadoOperaciones.INICIAR);
	        flux.subscribe(t -> {
	        	Novedad novedad = new Novedad();
	        	novedad.setNombreNovedad(planillaInfoModel.getNovedadModel().getNombreNovedad());
	        	novedad.setDescripcionNovedad(planillaInfoModel.getNovedadModel().getDescripcionNovedad());
	        	novedad.setFechaNovedad(new Date());
	        	novedad.setIdUsuarioReporta(planillaInfoModel.getNovedadModel().getIdUsuarioReporta());
	        	novedad.setPlanillaHoraria(planillaHoraria);
	            Mono<Novedad> saved = Mono.just(novedadRepository.save(novedad));
	            saved.subscribe(nov -> {
	                stream.next(EstadoOperaciones.EXITO);
	                stream.complete();
	            }, error -> {
	                stream.next(EstadoOperaciones.ERROR);
	                stream.error(error);
	                stream.complete();
	            });
	        });
	    };
	    // async block
		return  Flux.create(estadoEmision);
	}

}

package com.stratio.app.notification.model;

public class PlanillaInfoModel {

	private String idPlanillaHoraria;
	private String fechaActual;
	private String horaEntrada;
	private String horaSalida;
	private String horaAusencia;
	private String horasTrabajadas;
	private String idEmpleado;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombreDepartamento;
	private NovedadModel novedadModel;

	public PlanillaInfoModel(String idPlanillaHoraria, String fechaActual, String horaEntrada, String horaSalida,
			String horaAusencia, String horasTrabajadas, String idEmpleado, String nombre, String apellidoPaterno,
			String apellidoMaterno, String nombreDepartamento) {
		this.idPlanillaHoraria = idPlanillaHoraria;
		this.fechaActual = fechaActual;
		this.horaEntrada = horaEntrada;
		this.horaSalida = horaSalida;
		this.horaAusencia = horaAusencia;
		this.horasTrabajadas = horasTrabajadas;
		this.idEmpleado = idEmpleado;
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.nombreDepartamento = nombreDepartamento;
	}

	public String getIdPlanillaHoraria() {
		return idPlanillaHoraria;
	}

	public void setIdPlanillaHoraria(String idPlanillaHoraria) {
		this.idPlanillaHoraria = idPlanillaHoraria;
	}

	public String getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}

	public String getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(String horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public String getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(String horaSalida) {
		this.horaSalida = horaSalida;
	}

	public String getHoraAusencia() {
		return horaAusencia;
	}

	public void setHoraAusencia(String horaAusencia) {
		this.horaAusencia = horaAusencia;
	}

	public String getHorasTrabajadas() {
		return horasTrabajadas;
	}

	public void setHorasTrabajadas(String horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

	public String getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getNombreDepartamento() {
		return nombreDepartamento;
	}

	public void setNombreDepartamento(String nombreDepartamento) {
		this.nombreDepartamento = nombreDepartamento;
	}

	public NovedadModel getNovedadModel() {
		return novedadModel;
	}

	public void setNovedadModel(NovedadModel novedadModel) {
		this.novedadModel = novedadModel;
	}
	
	

}

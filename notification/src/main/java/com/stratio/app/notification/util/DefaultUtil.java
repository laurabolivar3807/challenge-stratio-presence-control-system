package com.stratio.app.notification.util;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

public abstract class DefaultUtil {
	
	private DefaultUtil() {
		throw new IllegalStateException("Util class");
	}
	
	public static LocalDateTime convertDateToLocalDateTime(Date date) {
		return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
	}
	
	public static LocalTime convertDateToLocalTimeTo(Date date) {
		return convertDateToLocalDateTime(date).toLocalTime();
	}

}

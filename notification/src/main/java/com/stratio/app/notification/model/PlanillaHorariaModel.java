package com.stratio.app.notification.model;

import java.time.LocalTime;
import java.util.List;
import java.time.LocalDateTime;

/**
 * 
 * @author Luis Alberto López Lombana
 *
 */
public class PlanillaHorariaModel {

	private LocalDateTime fechaActual;
	private LocalTime horaEntrada;
	private LocalTime horaSalida;
	private Long horaAusencia;
	private Long horasTrabajadas;
	private Integer idEmpleado;
	private List<NovedadModel> novedades;
	private boolean vacaciones;

	public PlanillaHorariaModel() {
	}

	public PlanillaHorariaModel(LocalDateTime fechaActual, LocalTime horaEntrada, LocalTime horaSalida,
			Long horaAusencia, Long horasTrabajadas, List<NovedadModel> novedades, boolean vacaciones) {
		this.fechaActual = fechaActual;
		this.horaEntrada = horaEntrada;
		this.horaSalida = horaSalida;
		this.horaAusencia = horaAusencia;
		this.horasTrabajadas = horasTrabajadas;
		this.novedades = novedades;
		this.vacaciones = vacaciones;
	}

	public LocalDateTime getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(LocalDateTime fechaActual) {
		this.fechaActual = fechaActual;
	}

	public LocalTime getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(LocalTime horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public LocalTime getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(LocalTime horaSalida) {
		this.horaSalida = horaSalida;
	}

	public Long getHoraAusencia() {
		return horaAusencia;
	}

	public void setHoraAusencia(Long horaAusencia) {
		this.horaAusencia = horaAusencia;
	}

	public Long getHorasTrabajadas() {
		return horasTrabajadas;
	}

	public void setHorasTrabajadas(Long horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

	public Integer getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(Integer idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public List<NovedadModel> getNovedades() {
		return novedades;
	}

	public void setNovedades(List<NovedadModel> novedades) {
		this.novedades = novedades;
	}

	public boolean isVacaciones() {
		return vacaciones;
	}

	public void setVacaciones(boolean vacaciones) {
		this.vacaciones = vacaciones;
	}

}

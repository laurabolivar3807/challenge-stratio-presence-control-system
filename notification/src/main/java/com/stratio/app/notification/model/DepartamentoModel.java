package com.stratio.app.notification.model;

import java.util.List;

/**
 * 
 * @author Luis Alberto López Lombana
 *
 */
public class DepartamentoModel {

	private Integer idDepartamento;
	private String nombreDepartamento;
	private String descripcionDepartamento;
	private List<EmpleadoModel> empleados;

	public DepartamentoModel() {
	}

	public DepartamentoModel(Integer idDepartamento, String nombreDepartamento, String descripcionDepartamento,
			List<EmpleadoModel> empleados) {
		this.idDepartamento = idDepartamento;
		this.nombreDepartamento = nombreDepartamento;
		this.descripcionDepartamento = descripcionDepartamento;
		this.empleados = empleados;
	}

	public Integer getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(Integer idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	public String getNombreDepartamento() {
		return nombreDepartamento;
	}

	public void setNombreDepartamento(String nombreDepartamento) {
		this.nombreDepartamento = nombreDepartamento;
	}

	public String getDescripcionDepartamento() {
		return descripcionDepartamento;
	}

	public void setDescripcionDepartamento(String descripcionDepartamento) {
		this.descripcionDepartamento = descripcionDepartamento;
	}

	public List<EmpleadoModel> getEmpleados() {
		return empleados;
	}

	public void setEmpleados(List<EmpleadoModel> empleados) {
		this.empleados = empleados;
	}

}

package com.stratio.app.notification.service.impl;

import org.springframework.stereotype.Service;

import com.stratio.app.notification.entity.Persona;
import com.stratio.app.notification.service.PersonaService;

/**
 * 
 * @author Luis Alberto López Lombana
 *
 */
@Service("personaServiceImpl")
public class PersonaServiceImpl extends InjectionRepository implements PersonaService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1479523695180782368L;

	@Override
	public Persona guardarPersona(Persona persona) {
		return personaRepository.save(persona);
	}

}

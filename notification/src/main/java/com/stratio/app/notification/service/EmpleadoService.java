package com.stratio.app.notification.service;

import com.stratio.app.notification.entity.Empleado;
import com.stratio.app.notification.model.EmpleadoModel;

import reactor.core.publisher.Flux;

public interface EmpleadoService extends java.io.Serializable {

	Flux<EmpleadoModel> obtenerListadoEmpleados();
	
	Empleado guardarEmpleado(Empleado empleado);

	String accesoEmpleado(Integer idEmpleado);
	
	String salidaEmpleado(Integer idEmpleado);

}

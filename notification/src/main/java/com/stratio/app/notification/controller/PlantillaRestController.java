package com.stratio.app.notification.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stratio.app.notification.enumerador.EstadoOperaciones;
import com.stratio.app.notification.model.PlanillaInfoModel;
import com.stratio.app.notification.service.PlanillaHorariaService;

import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/plantillas")
public class PlantillaRestController {


	@Autowired
	@Qualifier("planillaHorariaServiceImpl")
	protected PlanillaHorariaService plantillaHorariaService;

	@GetMapping(path = "/consultarPlanillas", produces = "application/stream+json")
	public Flux<PlanillaInfoModel> consultarPlanillas() {
		return plantillaHorariaService.consultarPlanillas();
	}
	
	@PostMapping(path = "/recepcionNotificacion", produces = "application/stream+json")
	public Flux<EstadoOperaciones> recepcionNotificacion(@RequestBody PlanillaInfoModel planillaInfoModel) {
		return plantillaHorariaService.recepcionNotificacion(planillaInfoModel);
	}

	
}

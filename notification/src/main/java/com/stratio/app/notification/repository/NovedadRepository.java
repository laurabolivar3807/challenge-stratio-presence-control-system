package com.stratio.app.notification.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stratio.app.notification.entity.Novedad;


@Repository("novedadRepository")
public interface NovedadRepository extends JpaRepository<Novedad, Serializable> {
	
}

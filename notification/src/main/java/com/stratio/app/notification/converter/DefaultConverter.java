package com.stratio.app.notification.converter;

import java.util.ArrayList;
import java.util.List;

import com.stratio.app.notification.entity.Departamento;
import com.stratio.app.notification.entity.Empleado;
import com.stratio.app.notification.entity.Novedad;
import com.stratio.app.notification.entity.PlanillaHoraria;
import com.stratio.app.notification.model.DepartamentoModel;
import com.stratio.app.notification.model.EmpleadoModel;
import com.stratio.app.notification.model.NovedadModel;
import com.stratio.app.notification.model.PlanillaHorariaModel;
import com.stratio.app.notification.util.DefaultUtil;

public abstract class DefaultConverter {

	private DefaultConverter() {
		throw new IllegalStateException("Converter class");
	}

	public static EmpleadoModel convertEmpleadoToEmpleadoModel(Empleado e) {
		EmpleadoModel em = new EmpleadoModel();
		em.setIdEmpleado(e.getIdEmpleado());
		em.setNombre(e.getPersona().getNombre());
		em.setApellidoPaterno(e.getPersona().getApellidoPaterno());
		em.setApellidoMaterno(e.getPersona().getApellidoPaterno());
		em.setSexo(e.getPersona().getSexo());
		em.setEdad(e.getPersona().getEdad());
		em.setNumeroIdentificacion(e.getPersona().getNumeroIdentificacion());
		em.setDireccion(e.getPersona().getDireccion());
		em.setTelefono(e.getPersona().getTelefono());
		em.setCorreoElectronico(e.getPersona().getCorreoElectronico());
		em.setLugarNacimiento(e.getPersona().getLugarNacimiento());
		em.setFechaNacimiento(DefaultUtil.convertDateToLocalDateTime(e.getPersona().getFechaNacimiento()));
		em.setCargo(e.getCargo());
		em.setDepartamento(convertDepartamentoToDepartamentoModel(e.getDepartamento()));
		em.setPlanillaHoraria(convertPlanillaHorariaToPlanillaHorariaModel(e.getPlanillaHoraria()));
		em.setFechaIngreso(DefaultUtil.convertDateToLocalDateTime(e.getFechaIngreso()));
		em.setSueldo(e.getSueldo().doubleValue());
		return em;
	}

	private static DepartamentoModel convertDepartamentoToDepartamentoModel(Departamento d) {
		DepartamentoModel departamento = new DepartamentoModel();
		departamento.setIdDepartamento(d.getIdDepartamento());
		departamento.setNombreDepartamento(d.getNombreDepartamento());
		departamento.setDescripcionDepartamento(d.getDescripcionDepartamento());
		return departamento;
	}

	private static PlanillaHorariaModel convertPlanillaHorariaToPlanillaHorariaModel(PlanillaHoraria ph) {
		PlanillaHorariaModel planillaHoraria = new PlanillaHorariaModel();
		planillaHoraria.setFechaActual(DefaultUtil.convertDateToLocalDateTime(ph.getFechaActual()));
		planillaHoraria.setHoraEntrada(ph.getHoraEntrada());
		planillaHoraria.setHoraSalida(ph.getHoraSalida());
		planillaHoraria.setHoraAusencia(ph.getHoraAusencia());
		planillaHoraria.setHorasTrabajadas(ph.getHorasTrabajadas());
		planillaHoraria.setVacaciones(ph.getVacaciones());
		planillaHoraria.setIdEmpleado(ph.getIdEmpleado());
		List<NovedadModel> lstNovedadModel = new ArrayList<>();
		ph.getNovedades().forEach(n -> lstNovedadModel.add(DefaultConverter.convertNovedadToNovedadModel(n)));
		planillaHoraria.setNovedades(lstNovedadModel);
		return planillaHoraria;
	}

	private static NovedadModel convertNovedadToNovedadModel(Novedad n) {
		NovedadModel novedadModel = new NovedadModel();
		novedadModel.setNombreNovedad(n.getNombreNovedad());
		novedadModel.setDescripcionNovedad(n.getDescripcionNovedad());
		novedadModel.setFechaNovedad(DefaultUtil.convertDateToLocalDateTime(n.getFechaNovedad()));
		novedadModel.setIdUsuarioReporta(n.getIdUsuarioReporta());
		return novedadModel;
	}

}

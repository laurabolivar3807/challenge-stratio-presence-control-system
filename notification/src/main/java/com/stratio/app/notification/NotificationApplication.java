package com.stratio.app.notification;

import java.util.ArrayList;
import java.util.Date;
import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.stratio.app.notification.entity.Empleado;
import com.stratio.app.notification.entity.Persona;
import com.stratio.app.notification.entity.Departamento;
import com.stratio.app.notification.entity.PlanillaHoraria;
import com.stratio.app.notification.service.DepartamentoService;
import com.stratio.app.notification.service.EmpleadoService;
import com.stratio.app.notification.service.PersonaService;
import com.stratio.app.notification.service.PlanillaHorariaService;

import io.netty.util.internal.ThreadLocalRandom;
/**
 * 
 * @author Luis Alberto López Lombana
 *
 */
@SpringBootApplication
public class NotificationApplication implements CommandLineRunner{
	
	private static final Date FECHA_ACTUAL = new Date();
	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationApplication.class);
	@Autowired
	@Qualifier("empleadoServiceImpl")
	protected EmpleadoService empleadoService;
	
	@Autowired
	@Qualifier("departamentoServiceImpl")
	protected DepartamentoService departamentoService;
	
	@Autowired
	@Qualifier("personaServiceImpl")
	protected PersonaService personaService;
	
	@Autowired
	@Qualifier("planillaHorariaServiceImpl")
	protected PlanillaHorariaService planillaHorariaService;

	public static void main(String[] args) {
		SpringApplication.run(NotificationApplication.class, args);
	}
	
	/**
	 * Simulación de creación de usuarios. Pre carga de datos para ejecución de test
	 */
	@Override
	public void run(String... args) throws Exception {
	
		int cont = 1;
		for(int i = 1; i<=10;i++) {
			Departamento deptCreado = persistInfoDepartamento();
			Persona persona = infoPersona(i,cont);
			Persona perCreado = personaService.guardarPersona(persona);
			PlanillaHoraria planHoraCreado = persistInfoPlanillaHoraria();	
			Empleado empleadoCreado=persistInfoEmpleado(i, deptCreado, perCreado,planHoraCreado);
			planHoraCreado.setIdEmpleado(empleadoCreado.getIdEmpleado());
			planillaHorariaService.guardarPlanillaHoraria(planHoraCreado);
			
		}
	}

	private PlanillaHoraria persistInfoPlanillaHoraria() {
		PlanillaHoraria planillaHoraria = new PlanillaHoraria();
		planillaHoraria.setUsuarioCreacion(ThreadLocalRandom.current().nextInt(123456, 789101));
		planillaHoraria.setFechaCreacion(FECHA_ACTUAL);
		planillaHoraria.setUsuarioModificacion(ThreadLocalRandom.current().nextInt(123456, 789101));
		planillaHoraria.setFechaModificacion(FECHA_ACTUAL);
		planillaHoraria.setNovedades(new ArrayList<>());
		return planillaHorariaService.guardarPlanillaHoraria(planillaHoraria);
	}

	private Empleado persistInfoEmpleado(int i, Departamento deptCreado, Persona perCreado,PlanillaHoraria planHoraCreado) {
		Empleado empleado = new Empleado();
		empleado.setPersona(perCreado);
		empleado.setCargo("cargo "+i);
		empleado.setDepartamento(deptCreado);
		empleado.setPlanillaHoraria(planHoraCreado);
		empleado.setFechaIngreso(FECHA_ACTUAL);
		empleado.setSueldo(new BigDecimal(ThreadLocalRandom.current().nextInt(8012457, 10000000)));
		empleado.setUsuarioCreacion(ThreadLocalRandom.current().nextInt(123456, 789101));
		empleado.setFechaCreacion(FECHA_ACTUAL);
		empleado.setUsuarioModificacion(ThreadLocalRandom.current().nextInt(123456, 789101));
		empleado.setFechaModificacion(FECHA_ACTUAL);
		Empleado res = empleadoService.guardarEmpleado(empleado);
		LOGGER.info(res.toString());
		return res;
	}

	private Departamento persistInfoDepartamento() {
		Departamento departamento = new Departamento();
		departamento.setNombreDepartamento("area "+1);
		departamento.setDescripcionDepartamento("Por asignar");
		departamento.setUsuarioCreacion(ThreadLocalRandom.current().nextInt(123456, 789101));
		departamento.setFechaCreacion(FECHA_ACTUAL);
		departamento.setUsuarioModificacion(ThreadLocalRandom.current().nextInt(123456, 789101));
		departamento.setFechaModificacion(FECHA_ACTUAL);
		return departamentoService.guardarDepartamento(departamento);
	}

	private Persona infoPersona(int i, int cont) {
		Persona persona = new Persona();
		persona.setNombre("Luis Alberto "+i);
		persona.setApellidoPaterno("López "+i);
		persona.setApellidoMaterno("Lombana "+i);
		persona.setSexo("M");
		persona.setEdad(ThreadLocalRandom.current().nextInt(18, 50));
		Integer identificacion= ThreadLocalRandom.current().nextInt(1035299942, 1953294441);
		persona.setNumeroIdentificacion(identificacion.toString());
		persona.setDireccion("Calle 70 # "+ i + " - " + cont+5 );
		Long telefono= ThreadLocalRandom.current().nextLong(3065554664L, 3106656940L);
		persona.setTelefono(telefono.toString());
		persona.setCorreoElectronico("lall"+i+"@gmail.com");
		persona.setLugarNacimiento("B/bermeja");
		persona.setFechaNacimiento(FECHA_ACTUAL);
		persona.setUsuarioCreacion(ThreadLocalRandom.current().nextInt(123456, 789101));
		persona.setFechaCreacion(FECHA_ACTUAL);
		persona.setUsuarioModificacion(ThreadLocalRandom.current().nextInt(123456, 789101));
		persona.setFechaModificacion(FECHA_ACTUAL);
		return persona;
	}
}

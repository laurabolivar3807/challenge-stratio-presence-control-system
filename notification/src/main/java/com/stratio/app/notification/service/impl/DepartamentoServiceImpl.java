package com.stratio.app.notification.service.impl;

import org.springframework.stereotype.Service;

import com.stratio.app.notification.entity.Departamento;
import com.stratio.app.notification.service.DepartamentoService;

@Service("departamentoServiceImpl")
public class DepartamentoServiceImpl  extends InjectionRepository implements DepartamentoService  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5092800016194179600L;

	@Override
	public Departamento guardarDepartamento(Departamento departamento) {
		return departamentoRepository.save(departamento);
	}

}

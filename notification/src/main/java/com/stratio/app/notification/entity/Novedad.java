package com.stratio.app.notification.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the novedad database table.
 * 
 */
@Entity
@NamedQuery(name="Novedad.findAll", query="SELECT n FROM Novedad n")
public class Novedad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_novedad")
	private Integer idNovedad;

	@Column(name="descripcion_novedad")
	private String descripcionNovedad;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="fecha_creacion")
	private Date fechaCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="fecha_modificacion")
	private Date fechaModificacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="fecha_novedad")
	private Date fechaNovedad;

	@Column(name="id_usuario_reporta")
	private Integer idUsuarioReporta;

	@Column(name="nombre_novedad")
	private String nombreNovedad;

	@Column(name="usuario_creacion")
	private Integer usuarioCreacion;

	@Column(name="usuario_modificacion")
	private Integer usuarioModificacion;

	//bi-directional many-to-one association to PlanillaHoraria
	@ManyToOne
	@JoinColumn(name="id_planilla_horaria")
	private PlanillaHoraria planillaHoraria;

	public Novedad() {
	}

	public Integer getIdNovedad() {
		return this.idNovedad;
	}

	public void setIdNovedad(Integer idNovedad) {
		this.idNovedad = idNovedad;
	}

	public String getDescripcionNovedad() {
		return this.descripcionNovedad;
	}

	public void setDescripcionNovedad(String descripcionNovedad) {
		this.descripcionNovedad = descripcionNovedad;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Date getFechaNovedad() {
		return this.fechaNovedad;
	}

	public void setFechaNovedad(Date fechaNovedad) {
		this.fechaNovedad = fechaNovedad;
	}

	public Integer getIdUsuarioReporta() {
		return this.idUsuarioReporta;
	}

	public void setIdUsuarioReporta(Integer idUsuarioReporta) {
		this.idUsuarioReporta = idUsuarioReporta;
	}

	public String getNombreNovedad() {
		return this.nombreNovedad;
	}

	public void setNombreNovedad(String nombreNovedad) {
		this.nombreNovedad = nombreNovedad;
	}

	public Integer getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(Integer usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Integer getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(Integer usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public PlanillaHoraria getPlanillaHoraria() {
		return this.planillaHoraria;
	}

	public void setPlanillaHoraria(PlanillaHoraria planillaHoraria) {
		this.planillaHoraria = planillaHoraria;
	}

}
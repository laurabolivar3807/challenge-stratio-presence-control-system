package com.stratio.app.notification.service;

import com.stratio.app.notification.entity.Departamento;

public interface DepartamentoService extends java.io.Serializable {
	Departamento guardarDepartamento(Departamento departamento);
}

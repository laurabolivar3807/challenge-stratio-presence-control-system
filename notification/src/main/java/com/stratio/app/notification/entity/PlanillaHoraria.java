package com.stratio.app.notification.entity;

import java.io.Serializable;
import java.time.LocalTime;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the planilla_horaria database table.
 * 
 */
@Entity
@Table(name = "planilla_horaria")
@NamedQuery(name = "PlanillaHoraria.findAll", query = "SELECT p FROM PlanillaHoraria p")
public class PlanillaHoraria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_planilla_horaria")
	private Integer idPlanillaHoraria;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_actual")
	private Date fechaActual;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_creacion")
	private Date fechaCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_modificacion")
	private Date fechaModificacion;

	@Column(name = "hora_ausencia")
	private Long horaAusencia;

	@Column(name = "hora_entrada")
	private LocalTime horaEntrada;

	@Column(name = "hora_salida")
	private LocalTime horaSalida;

	@Column(name = "horas_trabajadas")
	private Long horasTrabajadas;

	@Column(name = "id_empleado")
	private Integer idEmpleado;

	@Column(name = "usuario_creacion")
	private Integer usuarioCreacion;

	@Column(name = "usuario_modificacion")
	private Integer usuarioModificacion;

	private Boolean vacaciones;

	// bi-directional many-to-one association to Empleado
	@OneToMany(mappedBy = "planillaHoraria")
	private List<Empleado> empleados;

	// bi-directional many-to-one association to Novedad
	@OneToMany(mappedBy = "planillaHoraria")
	private List<Novedad> novedades;

	public PlanillaHoraria() {
	}

	public Integer getIdPlanillaHoraria() {
		return this.idPlanillaHoraria;
	}

	public void setIdPlanillaHoraria(Integer idPlanillaHoraria) {
		this.idPlanillaHoraria = idPlanillaHoraria;
	}

	public Date getFechaActual() {
		return this.fechaActual;
	}

	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Long getHoraAusencia() {
		return this.horaAusencia;
	}

	public void setHoraAusencia(Long horaAusencia) {
		this.horaAusencia = horaAusencia;
	}

	public LocalTime getHoraEntrada() {
		return this.horaEntrada;
	}

	public void setHoraEntrada(LocalTime horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public LocalTime getHoraSalida() {
		return this.horaSalida;
	}

	public void setHoraSalida(LocalTime horaSalida) {
		this.horaSalida = horaSalida;
	}

	public Long getHorasTrabajadas() {
		return this.horasTrabajadas;
	}

	public void setHorasTrabajadas(Long horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

	public Integer getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(Integer idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public Integer getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(Integer usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Integer getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(Integer usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public Boolean getVacaciones() {
		return this.vacaciones;
	}

	public void setVacaciones(Boolean vacaciones) {
		this.vacaciones = vacaciones;
	}

	public List<Empleado> getEmpleados() {
		return this.empleados;
	}

	public void setEmpleados(List<Empleado> empleados) {
		this.empleados = empleados;
	}

	public Empleado addEmpleado(Empleado empleado) {
		getEmpleados().add(empleado);
		empleado.setPlanillaHoraria(this);

		return empleado;
	}

	public Empleado removeEmpleado(Empleado empleado) {
		getEmpleados().remove(empleado);
		empleado.setPlanillaHoraria(null);

		return empleado;
	}

	public List<Novedad> getNovedades() {
		return this.novedades;
	}

	public void setNovedades(List<Novedad> novedades) {
		this.novedades = novedades;
	}

	public Novedad addNovedad(Novedad novedad) {
		getNovedades().add(novedad);
		novedad.setPlanillaHoraria(this);

		return novedad;
	}

	public Novedad removeNovedad(Novedad novedad) {
		getNovedades().remove(novedad);
		novedad.setPlanillaHoraria(null);

		return novedad;
	}

}
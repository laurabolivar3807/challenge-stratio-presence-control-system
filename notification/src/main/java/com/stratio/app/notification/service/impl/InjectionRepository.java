package com.stratio.app.notification.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.stratio.app.notification.repository.DepartamentoRepository;
import com.stratio.app.notification.repository.EmpleadoRepository;
import com.stratio.app.notification.repository.NovedadRepository;
import com.stratio.app.notification.repository.PersonaRepository;
import com.stratio.app.notification.repository.PlanillaHorariaRepository;

/**
 * 
 * @author Luis Alberto López Lombana
 *
 */
public class InjectionRepository {

	@Autowired
	@Qualifier("empleadoRepository")
	protected EmpleadoRepository empleadoRepository;
	
	@Autowired
	@Qualifier("departamentoRepository")
	protected DepartamentoRepository departamentoRepository;
	
	@Autowired
	@Qualifier("personaRepository")
	protected PersonaRepository personaRepository;

	@Autowired
	@Qualifier("planillaHorariaRepository")
	protected PlanillaHorariaRepository planillaHorariaRepository;
	
	@Autowired
	@Qualifier("novedadRepository")
	protected NovedadRepository novedadRepository;
	
	public InjectionRepository() {
		super();
	}

}
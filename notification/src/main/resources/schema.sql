DROP TABLE IF EXISTS persona CASCADE;
DROP TABLE IF EXISTS empleado CASCADE;
DROP TABLE IF EXISTS departamento CASCADE;
DROP TABLE IF EXISTS planilla_horaria CASCADE;
DROP TABLE IF EXISTS novedad CASCADE;

create table persona
(
  	id_persona integer not null,
	nombre VARCHAR(50) not null,
	apellido_paterno VARCHAR(50) not null,
	apellido_materno VARCHAR(50) not null,
	sexo VARCHAR(1) not null,
	edad integer,
	numero_identificacion VARCHAR(50) not null,
	direccion VARCHAR(50) not null,
	telefono VARCHAR(20),
	correo_electronico VARCHAR(100),
	lugar_nacimiento VARCHAR(50) not null,
	fecha_nacimiento timestamp not null,
	usuario_creacion integer not null default 0,
	fecha_creacion timestamp not null default now(),
	usuario_modificacion integer not null default 0,
	fecha_modificacion timestamp not null default now(),
	primary key(id_persona)
);


create table departamento(
	id_departamento integer not null,
	nombre_departamento  VARCHAR(50)  not null,
	descripcion_departamento VARCHAR(200)  not null,
	usuario_creacion integer not null default 0,
	fecha_creacion timestamp not null default now(),
	usuario_modificacion integer not null default 0,
	fecha_modificacion timestamp not null default now(),
	primary key(id_departamento)
);

create table planilla_horaria(
	id_planilla_horaria integer not null,
	id_empleado integer not null,
	fecha_actual timestamp not null,
	hora_entrada timestamp not null,
	hora_salida timestamp not null,
	hora_ausencia timestamp not null,
	horas_trabajadas timestamp not null,
	vacaciones boolean not null,
	usuario_creacion integer not null default 0,
	fecha_creacion timestamp not null default now(),
	usuario_modificacion integer not null default 0,
	fecha_modificacion timestamp not null default now(),
	primary key(id_planilla_horaria)
	
);

create table empleado(
	id_empleado integer not null,
	id_persona integer not null,
	id_departamento integer not null,
	id_planilla_horaria integer not null,
	cargo VARCHAR(50) not null,
	fecha_ingreso timestamp,
	sueldo decimal not null,
	usuario_creacion integer not null default 0,
	fecha_creacion timestamp not null default now(),
	usuario_modificacion integer not null default 0,
	fecha_modificacion timestamp not null default now(),
	primary key(id_empleado),
	foreign key(id_persona) REFERENCES persona(id_persona),
	foreign key(id_departamento) REFERENCES departamento(id_departamento),
	foreign key(id_planilla_horaria) REFERENCES planilla_horaria(id_planilla_horaria)
);

create table novedad(
	id_novedad integer not null,
	id_usuario_reporta integer not null,
	id_planilla_horaria integer not null,
	nombre_novedad VARCHAR(50)  not null,
	descripcion_novedad VARCHAR(50)  not null,
	fecha_novedad timestamp not null,
	usuario_creacion integer not null default 0,
	fecha_creacion timestamp not null default now(),
	usuario_modificacion integer not null default 0,
	fecha_modificacion timestamp not null default now(),
	primary key(id_novedad),
	foreign key(id_planilla_horaria) REFERENCES planilla_horaria(id_planilla_horaria)
	
);


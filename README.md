Aplicación Spring Boot

En este repositorio se tienen los siguientes artefactos:

1. Proyecto notification
2. Documento Presence control system.docx 
	-- Por favor hacer lectura lo realizado en el proyecto.
3. DiagramaDeClases.pdf
	-- Incluido además en el documento del punto 2, se generó con la aplicación Visual Paradigm (Contiene marca de texto sobre el diagrama pero es posible su lectura)
4. Empaquetado notification-0.0.1-SNAPSHOT.war
	-- Este proyecto puede ser ejecutado desde consola de comandos.
	   Comando --> java -jar notification-0.0.1-SNAPSHOT.war